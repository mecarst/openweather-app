import path  from 'path'
import webpack from 'webpack';

module.exports = {
    entry: {
        app: ['babel-polyfill', path.join(__dirname, '../client/js/index.js')],
        vendor: ['angular', 'angular-route'],
        styles: path.join(__dirname, '../client/styles/main.styl')
    },
    output: {
        path: path.join(__dirname, '../public/'),
        filename: 'js/[name].bundle.js',
        publicPath: './'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['env']
                }
            }, 
            {
                test: /\.html/, 
                exclude: /(node_modules)/,  
                use: {
                    loader: 'html-loader'
                }
            },
            {
                test: /\.json$/,
                use: ['json-loader']
            }, 
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                loader: 'url-loader?limit=4000&publicPath=./&name=imgs/[name].[ext]'
            }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'js/vendor.bundle.js' })
    ],
    resolve: {
        extensions: ['.js', '.styl']
    },
    devtool: 'eval-source-map'
};
