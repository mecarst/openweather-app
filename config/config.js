module.exports = {
    env: process.env.NODE_ENV || 'development',
    appPort: process.env.PORT || 3006,
    appID: process.env.APPID || '69b18809d74f17311fffd6c0973a7396',
    apiUri: process.env.APIURI || 'api',
    weatherAPIUri: process.env.WeatherAPIUri || 'http://api.openweathermap.org/data/2.5'
};
