### AngularJS 1.x based weather app 

- an weather application in angular running ExpressJS for READ operations
- the application uses yarn as an npm 

## Install 
- `yarn install`

## How to run prod
- `yarn start`
	- go to `localhost:3006`

## How to run local development
- `yarn start:dev`
- it will start the server in watch mode but not for the client application

### Local development client
- open a bash/cmd and run `yarn serve` starts webpack-dev-server
- and `yarn start:dev` starts the express server

I would suggets that you check package.json -> scripts object for better understanding on running the app.
