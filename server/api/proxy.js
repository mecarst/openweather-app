const router = require('express').Router();
const http = require('http');
const url = require('url');
const path = require ('path');

const { weatherAPIUri, appID } = require(path.join(__dirname + '/../../config/config'));

exports.copyHeaders = (source, target) => {
    for (let header in source.headers) {
        target.setHeader(header, source.headers[header]);
    }
};

router.all('/api/:resource(weather|forecast|city)*', (req, res) => {
    const { query: { q }, params: { resource }, method, originalUrl } = req;
	const { hostname, pathname, port, query } = url.parse(`${weatherAPIUri}/${resource}/?q=${q}&appid=${appID}&lang=en&mode=json&units=metric&cnt=7`);
    const serverRequest = http.request({
        port, hostname, method,
        path: `${pathname}?${query}`
    });

    exports.copyHeaders(req, serverRequest);

    serverRequest.on('response', serverResponse => {
        res.statusCode = serverResponse.statusCode;
        exports.copyHeaders(serverResponse, res);
        serverResponse.pipe(res);
    });

    serverRequest.on('error', err => {
        console.log(err)
        res.end();
    });

    req.pipe(serverRequest);
});

module.exports = router;
