const router = require('express').Router();
const weather = require('./weather');
const proxy = require('./proxy');

router.use(weather);
router.use(proxy);

module.exports = router;
