const router = require('express').Router();
const path = require ('path');
const { apiUri } = require(path.join(__dirname + '/../../../config/config'));

router
	.get('/', (req, res) => {
		res.render('index', { 
			title: 'Weather app', 
			ApiUri: apiUri
		});
	});

module.exports = router;
