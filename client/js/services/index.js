import WeatherAPIService from './weather.service';

export default angular
	.module('weatherApp.service', [])
	.service('weahterApi', WeatherAPIService)
	.name;
