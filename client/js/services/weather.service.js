export default class WeatherAPIService {
	constructor($http) {
		"ngInject";

		this.http = $http;
	}

	getCities(city) {
		return this.http.get(`${ApiUri}/api/weather?q=${city}`)
	        .then(({data}) => data)
	        .catch(err => err);
	}

	getCityForcast(city) {
		return this.http.get(`${ApiUri}/api/forecast/daily?q=${city}`)
	        .then(({data}) => data)
	        .catch(err => err);
	}
}