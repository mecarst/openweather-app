import AngularUiRouter from '@uirouter/angularjs';
import Components from './components';
import Services from './services';

const AppModules = [
	AngularUiRouter,
	Services,
	Components
]

export default AppModules;
