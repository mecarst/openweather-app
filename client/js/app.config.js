function AppConfig ($stateProvider, $urlRouterProvider) {
	"ngInject";

	$urlRouterProvider.otherwise('/');
	
	$stateProvider
		.state('home', {
			url: '/',
			component: 'content'
		})
		.state('forecast', {
			url: '/forecast',
			component: 'forecast',
			resolve: {
		      	city: ['weahterApi', (weahterApi) => weahterApi.getCityForcast(cities[0]).then(res => res)]
		  	}
		});
}

const cities = ['Amsterdam', 'London', 'Berlin', 'Timisoara', 'Rome'];

module.exports = {
	cities: cities,
	AppConfig: AppConfig
}