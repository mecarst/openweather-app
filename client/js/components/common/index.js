import Header from './header';

export default angular
	.module('common.component', [
		Header
	]).name;
