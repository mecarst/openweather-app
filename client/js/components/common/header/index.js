import HeaderComponent from './component';

export default angular
	.module('header.Module', [])
	.component('headerComponent', HeaderComponent)
	.name;
