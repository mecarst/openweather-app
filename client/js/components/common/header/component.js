import HeaderTpl from './template.html';
import HeaderCtrl from './controller';

const HeaderComponent = {
	controller: HeaderCtrl,
	template: HeaderTpl
}

export default HeaderComponent;