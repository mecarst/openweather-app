import Home from './home';
import Forecast from './forecast';
import Common from './common';

export default angular
	.module('weatherApp.component', [
		Home,
		Forecast,
		Common
	]).name;
