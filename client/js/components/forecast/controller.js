import { cities } from '../../app.config';

export default class ForecastCtrl {
	constructor($element, $scope, weahterApi, $q) {
		"ngInject";

		this.element = $element;
		this.weatherAPIService = weahterApi;
		this.cities = cities;
	}

	$onInit() {
		this.element.addClass('forecast');
  	}

  	setLocation(city) {
  		const res = this.weatherAPIService.getCityForcast(city).then(res => res);
  		res.then(city => this.city = city);
  	}
} 