import ForecastComponent from './component';

export default angular
	.module('forecast.Module', [])
	.component('forecast', ForecastComponent)
	.name;
