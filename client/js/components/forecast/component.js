import ForecastTpl from './template.html';
import ForecastCtrl from './controller';

const ForecastComponent = {
	bindings: {
		city: '<'
	},
	controller: ForecastCtrl,
	template: ForecastTpl
}

export default ForecastComponent;