import ContentComponent from './component';

export default angular
	.module('content.Module', [])
	.component('content', ContentComponent)
	.name;
