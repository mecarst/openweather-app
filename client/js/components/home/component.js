import ContentTpl from './template.html';
import ContentCtrl from './controller';

const ContentComponent = {
  	bindings: {},
	controller: ContentCtrl,
	template: ContentTpl
}

export default ContentComponent;