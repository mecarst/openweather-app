import { cities } from '../../app.config';

export default class ContentCtrl {
	constructor($scope, weahterApi, $q) {
		"ngInject";

		this.q = $q;
		this.weatherAPIService = weahterApi;
		this.cityData = [];
	}

	$onInit() {
		this.promises = cities.map(city =>this.weatherAPIService.getCities(city).then(res => res));
		this.q.all(this.promises).then(cities => this.setData(cities));
  	}

  	setData(cities) {
  		return cities.forEach(city => {
			this.cityData.push({
				name: city.name,
				weather: city.weather[0],
				temp: city.main.temp,
				tempMin: city.main.temp_min,
				tempHigh: city.main.temp_max,
				pressure: city.main.pressure,
				humidity: city.main.humidity,
				country: city.sys.country,
				wind: city.wind.speed
			});
		});
  	}
} 