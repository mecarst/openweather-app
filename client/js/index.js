import angular from 'angular';
import AppModules from './app.modules';
import {AppConfig} from './app.config';

let app =  angular
	.module('app', AppModules)
	.config(AppConfig);

export default app;